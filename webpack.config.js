module.exports = {
    resolve: {
        fallback: {
            'timers': require.resolve('timers-browserify'),
            'buffer': require.resolve('buffer/')
        }
    },
};