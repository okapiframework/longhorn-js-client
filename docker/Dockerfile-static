# syntax=docker/dockerfile:1

# Stage 1: build the bundle
FROM node:lts-alpine AS build
ARG PUBLIC_URL
ARG REACT_APP_LONGHORN_URL

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
COPY ../package.json .
COPY ../package-lock.json .

RUN npm install

COPY ../src ./src
COPY ../public ./public
COPY ../webpack.config.js ./

# Generate the build of the application
RUN npm run build

# Stage 2: Serve the bundle
FROM node:lts-alpine

RUN npm install --global serve

RUN mkdir -p "/opt/serve/longhorn-js-client" && chown -R node:node "/opt/serve/longhorn-js-client"

USER node

WORKDIR /opt/serve

COPY --from=build /usr/src/app/build /opt/serve/longhorn-js-client

EXPOSE 3000

CMD [ "serve", "--cors", "--no-port-switching", "--debug", "/opt/serve" ]