upstream longhorn-backend {
  server host.docker.internal:8080 ;
}

upstream node-dev-backend {
  server host.docker.internal:3000 ;
}

upstream node-static-backend {
  server host.docker.internal:3030 ;
}

server {
  listen                            80;
  server_name                       localhost;

  client_max_body_size              512M;

  location ~* "/Okapi-Longhorn-Files.*" {
    root                            /usr/share/nginx/html/;
    autoindex                       on;
    autoindex_localtime             on;
    autoindex_exact_size            off;
    autoindex_format                html;
    sendfile                        on;
    sendfile_max_chunk              1m;
  }

  location ~* "/okapi-longhorn.*" {
    proxy_pass                      http://longhorn-backend;

    proxy_read_timeout              3600s;

    add_header 'Access-Control-Allow-Origin' "*" always;
    add_header 'Access-Control-Allow-Methods' 'GET, PUT, POST, DELETE, OPTIONS, HEAD' always;
  }

  location /longhorn-js-client {
    proxy_pass                      http://node-static-backend;

    proxy_read_timeout              600s;

    proxy_http_version              1.1;
    proxy_set_header                Upgrade $http_upgrade;
    proxy_set_header                Connection "upgrade";
  }

  location / {
    proxy_pass                      http://node-dev-backend;

    proxy_read_timeout              600s;

    proxy_http_version              1.1;
    proxy_set_header                Upgrade $http_upgrade;
    proxy_set_header                Connection "upgrade";
  }
}