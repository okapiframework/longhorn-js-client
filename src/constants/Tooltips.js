import React from 'react';
import {Tooltip} from "react-bootstrap";

export default class Tooltips {
  static OPTIONAL = (
    <Tooltip id="tooltip">This field is optional<br/>Leave it empty to use the defaults</Tooltip>
  );

  static UNZIP_FILES = (
    <Tooltip id="tooltip">Click to unzip files</Tooltip>
  );

  static DO_NOT_UNZIP_FILES = (
    <Tooltip id="tooltip">Click to upload zip as input files</Tooltip>
  );
}