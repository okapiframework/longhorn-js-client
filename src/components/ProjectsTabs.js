import React, {Component} from 'react';
import {Badge, Col, Nav, NavItem} from 'react-bootstrap';
import xml2js from 'xml2js';
import _ from 'underscore';

import LonghornApi from '../constants/LonghornApi';

import ProjectTab from "./ProjectTab";
import Alerts from "./Alerts";


export default class ProjectsTabs extends Component {

  constructor(props) {
    super(props);
    this.state = {
      projects: [],
    };

    this.alertList = null;
    this.projectDetails = null;

    this.setAlertListRef = element => {
      this.alertList = element;
    };

    this.setProjectDetailsRef = element => {
      this.projectDetails = element;
    };
  }

  componentDidMount() {
    this.fetchProjects();
  }

  handleFetchErrors(response) {
    if (!response.ok) {
      response.text().then(text => {
        console.error('ProjectsTabs', response, text);
      });
      throw Error(`${response.statusText}`);
    }
    return response;
  }

  fetchProjects() {
    console.debug('fetchProjects', this.state.projects);
    let projects = [];
    fetch(this.props.longhornUrl + LonghornApi.PATHS.PROJECTS)
      .then(this.handleFetchErrors)
      .then(response => response.text())
      .then(response => xml2js.parseStringPromise(response, {}))
      .then((result) => {
        console.debug('fetchProjects result', result);
        if (_.has(result, 'l') && _.has(result.l, 'e')) {
          projects = result.l.e;
        }
        this.setState({
          projects: projects,
        });
        this.alertList.info({
          message: `${projects.length} projects found`,
        })
      })
      .catch((err) => {
        console.log('fetchProjects', err);
        this.alertList.error({
          headline: `Failed to load projects`,
          message: `${err.toString()}, check the browser console for details.`
        });
      });
  }

  createProject() {
    let project = ProjectTab.getProjectInitialState();
    fetch(this.props.longhornUrl + LonghornApi.PATHS.PROJECTS + LonghornApi.PATHS.NEW, {method: 'POST'})
      .then(this.handleFetchErrors)
      .then((response) => {
        console.debug('createProject result', response);
        const projectId = /([^\/]+$)/.exec(response?.headers?.get("Location"))?.at(0);
        project.id = projectId;
        this.setState({
          project: project,
        });
        this.alertList.clear();
        this.alertList.confirm({
          message: `Project ${projectId} created`,
        });
        this.fetchProjects();
      })
      .catch((err) => {
        console.error('createProject', err);
        this.alertList.error({
          headline: `Failed to create new project`,
          message: `${err.toString()}, check the browser console for details.`
        });
      });
  }

  render() {
    return (
      <div>
        <Alerts ref={this.setAlertListRef}/>
        <Col sm={3} md={2}>
          <Nav bsStyle="pills" stacked>
            <NavItem onClick={this.createProject.bind(this)}>
              New project
              <Badge pullRight><i className="fa fa-folder-o" aria-hidden="true"/></Badge>
            </NavItem>
            {
              this.state.projects
                  .sort((a, b) => {
                    return (/^[0-9]+$/.test(a)) ? parseInt(a) > parseInt(b): a.localeCompare(b);
                  })
                  .reverse()
                  .map((projectId, index) => (
                <NavItem eventKey={`project_${projectId}`}
                         key={`project_${index}`}
                         onClick={this.projectDetails.fetchProject.bind(this.projectDetails, projectId)}>
                  Project {projectId}
                  <Badge pullRight><i className="fa fa-folder" aria-hidden="true" /></Badge>
                </NavItem>
              ))
            }
          </Nav>
        </Col>
        <ProjectTab ref={this.setProjectDetailsRef}
                    projectsTabs={this}
                    alertList={this.alertList}
                    longhornUrl={this.props.longhornUrl}/>
      </div>
    );
  }
};