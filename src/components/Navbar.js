import React, {Component} from 'react';
import {MenuItem, Nav, Navbar, NavbarBrand, NavDropdown} from "react-bootstrap";

import LonghornApi from '../constants/LonghornApi';
import packageJson from '../../package.json';
import NavbarToggle from "react-bootstrap/lib/NavbarToggle";
import NavbarCollapse from "react-bootstrap/lib/NavbarCollapse";
import NavbarHeader from "react-bootstrap/lib/NavbarHeader";


export default class Header extends Component {

  constructor(props) {
    super(props);
    this.state = {
      status: {
        name: null,
        version: "Unknown"
      },
    };
  };

  componentDidMount() {
    this.fetchOkapiLonghornStatus();
  }

  fetchOkapiLonghornStatus() {
    console.debug('fetchOkapiLonghornStatus fetching', this.state.status);
    fetch(`${this.props.longhornUrl}/status.json`, {cache: "no-store"})
      .then(response => response.json())
      .then((response) => {
        console.debug('fetchOkapiLonghornStatus fetched', response);
        this.setState({
          status: response,
        });
      })
      .catch((err) => {
        console.warn('fetchOkapiLonghornStatus failed', err)
      });
  }

  render() {
    return (
      <Navbar inverse fixedTop>
        <NavbarHeader>
          <NavbarBrand>
            <a href={process.env.PUBLIC_URL} title={`Okapi Longhorn JS Client ${packageJson.version}`}>
              Okapi Longhorn JS Client
            </a>
          </NavbarBrand>
          <NavbarToggle/>
        </NavbarHeader>
        <NavbarCollapse>
          <Nav pullRight>
            <NavDropdown id="NavBarMenu" title="Info">
              <MenuItem header>Versions</MenuItem>
              <MenuItem disabled className="clearfix">
                <span className="pull-left">Client</span>
                <strong className="pull-right">{packageJson.version}</strong>
              </MenuItem>
              <MenuItem disabled className="clearfix">
                <span className="pull-left">API</span>
                <strong className="pull-right">{this.state.status.version}</strong>
              </MenuItem>
              <MenuItem divider />
              <MenuItem href={this.props.longhornUrl + LonghornApi.PATHS.PROJECTS} target="blank">
                Okapi Longhorn API
              </MenuItem>
              <MenuItem href="http://okapiframework.org/wiki/index.php?title=Longhorn" target="blank">
                Okapi Longhorn API Wiki
              </MenuItem>
              <MenuItem divider />
              <MenuItem href="https://bitbucket.org/okapiframework/longhorn-js-client" target="blank">
                <i className="fa fa-bitbucket" aria-hidden="true"/> Fork me on Bitbucket
              </MenuItem>
            </NavDropdown>
          </Nav>
        </NavbarCollapse>
      </Navbar>
    );
  }
};